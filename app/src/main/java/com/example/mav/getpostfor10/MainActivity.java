package com.example.mav.getpostfor10;

import android.app.Activity;
import android.app.LoaderManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;


public class MainActivity extends Activity {

    static final int LOADER_TIME_ID = 1;
    protected LoaderManager.LoaderCallbacks<String> loaderCallbacks; //fixme заимплементить для активити
    TextView mTextView;
    TextView secondText; //fixme неговорящее название
    Button btn;
    RadioGroup rgComaFormat; //fixme неговорящее название
    String coma; //fixme неговорящее название

    static int lastCheckedId = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        mTextView = (TextView) findViewById(R.id.txtview);
        secondText = (TextView) findViewById(R.id.secondText);
        rgComaFormat = (RadioGroup) findViewById(R.id.rg);
        btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (rgComaFormat.getCheckedRadioButtonId()==R.id.rdLong) {
                    coma = " / ";
                    secondText.clearComposingText();
                } else  {
                    coma = "\n";
                    secondText.clearComposingText();
                }
                    ;
                loaderCallbacks = new LoaderManager.LoaderCallbacks<String>() {
                    @Override
                    public android.content.Loader<String> onCreateLoader(int id, Bundle args) {
                        Log.d("MainActivity","onCreateLoader");
                        String cm = args.getString("key");
                        return new FriendLoader(MainActivity.this, cm);
                    }

                    @Override
                    public void onLoadFinished(android.content.Loader<String> loader, String data) {
                        secondText.setText(data);
                        Log.d("MainActivity","onLoadFinished");

                    }

                    @Override
                    public void onLoaderReset(android.content.Loader<String> loader) {

                    }
                };
                Bundle bundle = new Bundle();
                bundle.putString("key", coma);
                getLoaderManager().initLoader(LOADER_TIME_ID, bundle, loaderCallbacks);

                lastCheckedId = rgComaFormat.getCheckedRadioButtonId();

            }
        });
    }


}