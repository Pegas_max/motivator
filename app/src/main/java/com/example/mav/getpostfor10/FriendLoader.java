package com.example.mav.getpostfor10;

import android.content.Context;
import android.content.Loader;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by mav on 12.12.2014.
 */
public class FriendLoader extends Loader<String> {

    final String LOG_TAG = "LoaderLog";

    String coma; //fixme неговорящее название. какой квалификатор доступа
    DownloadJson downloadJson;
    private static final String MYNEWS_URL = "http://demo3956510.mockable.io/friends_test";


    public FriendLoader (Context context, String args){
        super(context);
        Log.d(LOG_TAG, hashCode() + "Create Friend Loader");
        if (args!= null)
            coma = args;
        if (TextUtils.isEmpty(coma))
            coma = args;
    }

    @Override
    protected void onStartLoading(){
        super.onStartLoading();
        Log.d(LOG_TAG, hashCode()+ " onStartLoading");
        onForceLoad(); //fixme лишнее

    }

    @Override
    protected void onStopLoading(){
        super.onStopLoading();
        Log.d(LOG_TAG, hashCode()+" omStopLoading");
    }

    @Override
    protected void onForceLoad (){
        super.onForceLoad();
        Log.d(LOG_TAG, hashCode()+ " onforceLoad");
        if (downloadJson != null)
            downloadJson.cancel(true);
        downloadJson = new DownloadJson();
        downloadJson.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, coma);

    }

    @Override
    protected void onAbandon(){
        super.onAbandon();
        Log.d(LOG_TAG, hashCode() + " onAbandon");
    }

    @Override
    protected void onReset(){
        super.onReset();
        Log.d(LOG_TAG, hashCode()+" onReset");
    }

    void getResultFromTask(String result){
        deliverResult(result);
        Log.d(LOG_TAG, hashCode()+" getResultFromTask");
        onStopLoading();
        
        onReset();
    }


    private class DownloadJson extends AsyncTask <String, Void, String> { //fixme неговорощее название класса

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(MYNEWS_URL);

                //создаем GET-запрос
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                //создание входного потока и буфера для записи результата
                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                //запись
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine())!=null) {
                    buffer.append(line);
                }

                resultJson = buffer.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultJson;
        }

        @Override
        protected void onPostExecute(String strJson) {
            super.onPostExecute(strJson);

            JSONObject dataJsonObj = null;
            String secondName = "Список друзей:"+coma;

            try { //fixme вынести логику в метод
                dataJsonObj = new JSONObject(strJson);
                JSONArray friends = dataJsonObj.getJSONArray("friends");

                //выводим все контакты из списка
                for (int i=0; i < friends.length(); i++) {
                    JSONObject friend = friends.getJSONObject(i);

                    JSONObject contacts = friend.getJSONObject("contacts");

                    String name  = friend.getString("name");
                    String city  = friend.getString("city");
                    String phone = contacts.getString("mobile");
                    String email = contacts.getString("email");
                    String skype = contacts.getString("skype");

                    secondName =           secondName + "\n" +
                                    "name: "  + name  + "\n" +
                                    "city:  " + city  + "\n" +
                                    "phone: " + phone + "\n" +
                                    "email: " + email + "\n" +
                                    "skype: " + skype + "\n" ;
                }
                Log.d(LOG_TAG, hashCode() + secondName);

            } catch (Exception e){
                e.printStackTrace();
                Log.e("Json: ","Format problem");
            }
            getResultFromTask(secondName);
        }

    };
}
